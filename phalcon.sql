/*
Navicat MySQL Data Transfer

Source Server         : 31.41.95.52
Source Server Version : 50546
Source Host           : localhost:3306
Source Database       : phalcon

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-06-15 07:16:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(10) unsigned zerofill NOT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `superior_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Ð”Ð¸Ð¼Ð°', 'Ð¤Ð¾Ð¼Ð°Ðº', 'michail_dobratov@mail.ru', '0678889526', '', null);
INSERT INTO `users` VALUES ('2', 'Kirill', 'Desnev', 'kirill_desnev@mail.ru', '0506895623', '', '1');
INSERT INTO `users` VALUES ('3', 'Michail', 'Dobratov', 'michail_dobratov@mail.ru', '0678889526', '', '2');
INSERT INTO `users` VALUES ('4', 'Сергей', 'Данчук', 'sergey_danchuk@mail.ru', '0986545789', '', null);
