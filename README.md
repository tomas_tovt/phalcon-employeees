# Phalcon Employees

### Requirements

To run this application on your machine, you need at least:

* PHP >= 5.4
* [Apache][2] Web Server with [mod_rewrite][3] enabled or [Nginx][4] Web Server
* Latest stable [Phalcon Framework release][5] extension enabled

## License

Phalcon Tutorial is open-sourced software licensed under the [New BSD License][6]. © Phalcon Framework Team and contributors

[1]: http://docs.phalconphp.com/en/latest/reference/tutorial.html
[2]: http://httpd.apache.org/
[3]: http://httpd.apache.org/docs/current/mod/mod_rewrite.html
[4]: http://nginx.org/
[5]: https://github.com/phalcon/cphalcon/releases
[6]: https://github.com/phalcon/tutorial/blob/master/docs/LICENSE.md
