<title>{{ title|e }}</title>
<div class="row">
	<div class="col-md-12">
	
	<div id="messages">{{ flash.output() }}</div>
	
	{{ form('users/add', 'method': 'post', 'class': 'form-horizontal', 'role':'form') }}

		<div class="form-group">
			<label class="col-sm-2 control-label">Имя</label>
			<div class="col-sm-10">
				{{ text_field("name", "size": 32, 'class': 'form-control', 'placeholder': 'Введите имя...', 'required': 'required' ) }}
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Фамилия</label>
			<div class="col-sm-10">
				{{ text_field("lastname", "size": 32, 'class': 'form-control', 'placeholder': 'Введите фамилию...', 'required': 'required' ) }}
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail</label>
			<div class="col-sm-10">
				{{ email_field("email", "size": 32, 'class': 'form-control', 'placeholder': 'Введите E-mail...', 'required': 'required' ) }}
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Номер телефона</label>
			<div class="col-sm-10">
				{{ text_field("phone", "size": 32, 'class': 'form-control', 'placeholder': 'Введите телефон...', 'required': 'required' ) }}
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заметки</label>
			<div class="col-sm-10">
				{{ text_area("notes", "cols": 32, 'rows': 5, 'class': 'form-control', 'placeholder': 'Заметки (Опционально)') }}
			</div>
		</div>
			
		{{ submit_button('Создать Пользователя') }}

	{{ end_form() }}
	</div>
</div>