<title><?php echo $this->escaper->escapeHtml($title); ?></title>
<div class="row">
	<div class="col-md-12">
	
	<div id="messages"><?php echo $this->flash->output(); ?></div>
	
	<?php echo $this->tag->form(array('users/add', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form')); ?>

		<div class="form-group">
			<label class="col-sm-2 control-label">Имя</label>
			<div class="col-sm-10">
				<?php echo $this->tag->textField(array('name', 'size' => 32, 'class' => 'form-control', 'placeholder' => 'Введите имя...', 'required' => 'required')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Фамилия</label>
			<div class="col-sm-10">
				<?php echo $this->tag->textField(array('lastname', 'size' => 32, 'class' => 'form-control', 'placeholder' => 'Введите фамилию...', 'required' => 'required')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">E-Mail</label>
			<div class="col-sm-10">
				<?php echo $this->tag->emailField(array('email', 'size' => 32, 'class' => 'form-control', 'placeholder' => 'Введите E-mail...', 'required' => 'required')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Номер телефона</label>
			<div class="col-sm-10">
				<?php echo $this->tag->textField(array('phone', 'size' => 32, 'class' => 'form-control', 'placeholder' => 'Введите телефон...', 'required' => 'required')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Заметки</label>
			<div class="col-sm-10">
				<?php echo $this->tag->textArea(array('notes', 'cols' => 32, 'rows' => 5, 'class' => 'form-control', 'placeholder' => 'Заметки (Опционально)')); ?>
			</div>
		</div>
			
		<?php echo $this->tag->submitButton(array('Создать Пользователя')); ?>

	<?php echo $this->tag->endForm(); ?>
	</div>
</div>