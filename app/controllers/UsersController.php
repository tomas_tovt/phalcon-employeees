<?php

use Phalcon\Mvc\Controller;
use Phalcon\Tag;

class UsersController extends Controller
{

	public function indexAction()
	{
		$response = new \Phalcon\Http\Response();
		$response->redirect("/users/list");
		return $response;		
	}
	
	public function listAction()
	{
		//Вывод древовидного либо выпадающего списка
	}
	
	public function addAction()
	{
		if ($this->request->isPost()) {
            $user = new Users();
            $user->assign(array(
                'name' => $this->request->getPost('name', 'striptags'),
                'lastname' => $this->request->getPost('lastname', 'striptags'),
                'email' => $this->request->getPost('email', 'email'),
				'phone' => $this->request->getPost('phone', 'striptags'),
				'notes' => $this->request->getPost('notes', 'striptags'),
            ));
            if (!$user->save()) {
            } else {
                $this->flash->success("Пользователь добавлен");
                Tag::resetInput();
            }
		}
		$this->view->title = "Cоздание пользователя";
		
	}
	
	public function editAction($uid)
	{
		if ($this->request->isGet()) {
			$user = Users::findFirst($uid);
			$this->view->name = $user->name;
			$this->view->lastname = $user->lastname;	
			$this->view->email = $user->email;	
			$this->view->phone = $user->phone;	
			$this->view->notes = $user->notes;	
		}
		if ($this->request->isPost()) {
            $user = Users::findFirst($uid);
            $user->assign(array(
                'name' => $this->request->getPost('name', 'striptags'),
                'lastname' => $this->request->getPost('lastname', 'striptags'),
                'email' => $this->request->getPost('email', 'email'),
				'phone' => $this->request->getPost('phone', 'striptags'),
				'notes' => $this->request->getPost('notes', 'striptags'),
            ));
            if (!$user->save()) {
                $this->flash->error($user->getMessages());
            } else {
                $this->flash->success("Пользователь изменен");
            }
		}
		$this->view->title = "Редактирование пользователя";	
	}
	
	public function deleteAction($uid)
	{
		if ($this->request->isPost()) {
            $user = Users::findFirst();
            if (!$user->delete()) {
                $this->flash->error($user->getMessages());
            } else {
                $this->flash->success("Пользователь удален");
            }
		}
	}
}	
